# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.7] (Xenial) - 2018-12-09
### Added
- (#11) Added the third option of being able to make the image writable temporarily, until the next reboot -- from `System > Make image writable`.
- (#13) Now able to manually restart Unity 8 (i.e. restart the home screen) -- from `System > Services`.
- German translation from [@Danfro](https://gitlab.com/Danfro) x2!
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).

### Changed
- Updated links due to the transfer of the repo to the new maintainer.

### Removed
- (#11) Rebooting no longer required when making the image writable.

## [0.4.6] (Xenial) - 2018-12-07
### Added
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).

## [0.4.5] (Xenial) - 2018-12-05
### Added
- (#10) SSH settings: toggle for enabling/disabling SSH.
- Changelog based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Changed
- Centralised app version number.

## [0.4.4] (Xenial) - 2018-11-17
- Updated the German translation

## [0.4.3] (Xenial)
- Updated the scale option
- Fix the sound notification
- Other small bugs fixed
- Updating translations

## [0.4.2] (Xenial)
- Few bug fixed
- Updating translations
- Adding support to change the SMS tone

## [0.4.1] (Xenial)
- Adding support for Morph Browser

## [0.3.91]
- Fixed issue with Unity8 settings visibility

## [0.3.90]
- Fix charset when reading .desktop files, thanks to Michael Zanetti
- [OTA-14] Added option to enable/disable the indicator menu, thanks to Brian Douglass
- [OTA-14] Added option to enable/disable the launcher, thanks to Brian Douglass
- Re-sorted entries for Unity 8 settings
- Updated translation template
