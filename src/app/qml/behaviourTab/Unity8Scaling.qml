/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import TweakTool 1.0
import Ubuntu.Components.Popups 1.3
import "../components"
import "../components/ListItems" as ListItems

Page {
    id: rootItem

    property int currentValue: units.gu(1)
    property int newValue: units.gu(1)
          
    header: PageHeader {
        title: i18n.tr("Scaling")
        flickable: view.flickableItem
    }

    function getPixel(guValue){
        return guValue * Math.floor(minPushSlider.value)
    }

    function applySetting(value, testOnly){
        var restoreDefault = (settings.defaultDeviceGU === value)
        var dialogMode

        switch(true){
        case testOnly:
            dialogMode = "TEST"
            break
        case restoreDefault:
            dialogMode = "DEFAULT"
            break
        default:
            if(rootItem.currentValue === rootItem.newValue){
                dialogMode = "APPLY"
            }else{
                dialogMode = "APPLYREBOOT"
            }
            break
        }

        var popup = PopupUtils.open(confirmDialog, rootItem, {"mode": dialogMode})

        popup.accepted.connect(function() {
            if (restoreDefault){
                deletePersistFile()
            }else if(!testOnly){
                createPersistFile()
            }
            
            //Sets the environment variable GRID_UNIT_PX and restarts unity 8 if necessary
            if(rootItem.currentValue !== rootItem.newValue || dialogMode === "DEFAULT"){
                Process.launch('/bin/sh -c "initctl set-env --global GRID_UNIT_PX=' + value + ' && restart unity8"')
            }
        })
    }

    function createPersistFile(){
        deletePersistFile()
        Process.launch('/bin/sh -c "echo \'start on starting dbus\nexec initctl set-env --global GRID_UNIT_PX=' + newValue + '\' > /home/phablet/.config/upstart/ut_tweak_gu.conf"')
    }
    function deletePersistFile(){
        Process.launch('rm /home/phablet/.config/upstart/ut_tweak_gu.conf')
    }

    ScrollView {
        id: view
        anchors.fill: parent

        Column {
            id: column

            width: view.width

            Rectangle{
                width: parent.width
                height: units.gu(40)

                Rectangle{
                    id: previewRec

                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors{
                        top: parent.top
                        topMargin: units.gu(2)
                    }
                    width: units.gu(30)
                    height: parent.height - previewLabel.height - anchors.topMargin

                    Column{
                        anchors{
                            top: parent.top
                            topMargin: units.gu(2)
                            left: parent.left
                            right: parent.right
                            bottom: parent.bottom
                        }

                        spacing: getPixel(2)
                        Row{
                            spacing: getPixel(1)
                            anchors.horizontalCenter: parent.horizontalCenter
                            Icon{
                                name: "ubuntu-logo-symbolic"
                                width: getPixel(3)
                                height: width
                                color: UbuntuColors.orange
                            }
                            Label{
                                text: i18n.tr("Large Text")
                                font.pixelSize: getPixel(2.5) //units.gu(3)
                            }
                        }


                        Button{
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: getPixel(15) //units.gu(15)
                            height: getPixel(4) //units.gu(4)
                            color: theme.palette.normal.positive

                            Label{
                                id: buttonLabel

                                anchors.fill: parent
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                text: i18n.tr("Normal Text")
                                color: theme.palette.normal.positiveText
                                font.pixelSize: getPixel(1.7) //units.gu(1.7)
//                                Large = 2.5 (2.47)
//                                Medium = 1.5 (1.7)
//                                XLarge = 3.5 (3.3)
                            }
                        }

                        TextArea{
                            text: i18n.tr("Small Text")  + ": Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum convallis mauris in aliquam blandit. Sed vel nulla id tortor dignissim commodo in et risus. Vestibulum vitae erat ipsum. Cras id imperdiet massa, in eleifend velit. "
                            anchors{
                                left: parent.left
                                right: parent.right
                                margins: getPixel(2) //units.gu(2)
                            }
                            font.pixelSize: getPixel(1.5) //units.gu(1.5)
                            readOnly: true
                            autoSize: true
                        }
                    }
                }
                Label{
                    id: previewLabel

                    text: i18n.tr("Preview (Approximation only)")
                    font.italic: true
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    width: parent.width
                    height: units.gu(5)
                    anchors.bottom: parent.bottom
                }
            }


            ListItems.SectionDivider { text: i18n.tr("Grid Unit") }

            ListItem {
                height: Math.max(implicitHeight, minpushlayout.height)

                ListItemLayout {
                    id: minpushlayout
                    anchors.centerIn: parent
                    title.text: rootItem.currentValue === rootItem.newValue ? i18n.tr("Current value: %1").arg(rootItem.currentValue) : i18n.tr("Current value: %1").arg(rootItem.currentValue) + " - <b>" + i18n.tr("New value: %1").arg(rootItem.newValue) + "</b>"
                    subtitle.text: i18n.tr("Unity 8 scaling in grid units.")
                    summary.text: i18n.tr("How much pixels should there be in 1 grid unit.")
                }
            }

            ListItem {
                height: units.gu(12)                

                Slider {
                    id: minPushSlider
                    anchors.horizontalCenter: parent.horizontalCenter
                    minimumValue: 10
                    maximumValue: 30
                    stepSize: 1
                    width: parent.width * 0.9

                    Component.onCompleted: {
                        value = units.gu(1)
                    }
                    onValueChanged: {
                        rootItem.newValue = Math.floor(value)
                    }
                }

                Row{
                    id: buttonsRow

                    spacing: units.gu(3)

                    anchors{
                        top: minPushSlider.bottom
                        horizontalCenter: parent.horizontalCenter
                        margins: units.gu(2)
                    }

                    Button {
                        id: testButton

                        enabled: rootItem.currentValue !== rootItem.newValue
                        color: UbuntuColors.orange

                        action: Action {
                            text: i18n.tr("Test")
                            onTriggered: {
                                applySetting(rootItem.newValue, true)
                            }
                        }
                    }

                    Button{
                        id: applyButton

                        text: i18n.tr("Apply")
                        color: theme.palette.normal.positive
                        enabled: rootItem.newValue !== settings.defaultDeviceGU
                        onClicked: {
                            applySetting(rootItem.newValue, false)
                        }
                    }

                }
            }

            ListItems.Control {
                visible: settings.defaultDeviceGU !== rootItem.currentValue
                Label {
                    text: i18n.tr("Restore default...")
                    anchors.verticalCenter: parent.verticalCenter
                }

                onClicked: {
                    applySetting(settings.defaultDeviceGU, false)
                }
            }
        }
    }

    Component {
        id: confirmDialog
        Dialog {
            id: confirmDialogue

            signal accepted
            property string mode
            property string extraNote: switch(mode){
                                       case "TEST":
                                           i18n.tr("This is just temporary and a device reboot will revert back to the default scaling")
                                           break
                                       case "APPLYREBOOT":
                                           i18n.tr("This will make the scaling persistent and will survive a device reboot")
                                           break
                                       case "DEFAULT":
                                           i18n.tr("This will restore to the default scaling")
                                           break
                                       default:
                                           ""
                                           break
                                       }

            title: mode === "APPLY" ? i18n.tr("Apply new setting") : i18n.tr("Restart Unity 8")
            text: mode === "APPLY" ? i18n.tr("This will make the scaling persistent and will survive a device reboot") : i18n.tr("This change requires restarting Unity 8 to take effect. Do you want to restart Unity 8 now? All currently opened apps will be closed. Save all your works before continuing!")
                  + (extraNote ? "<br><br>" + i18n.tr("Note:") + " " + extraNote : "")

            Button{
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(confirmDialogue)
            }

            Button{
                text: mode === "APPLY" ? i18n.tr("Proceed") : i18n.tr("Restart Unity 8")
                color: mode === "APPLY" ? theme.palette.normal.positive : theme.palette.normal.negative
                onClicked:  {
                    confirmDialogue.accepted()
                    PopupUtils.close(confirmDialogue)
                }
            }
        }
    }
}
